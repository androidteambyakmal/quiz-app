package com.example.android.quizapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    RadioGroup question1, question2, question4, question6;
    CheckBox question3_a, question3_b, question3_c, question3_d, question5_a, question5_b, question5_c, question5_d;
    int totalMarks = 6;
    int gaindMarks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Getting RadioGroup Objects
        question1 = (RadioGroup) findViewById(R.id.q1);
        question2 = (RadioGroup) findViewById(R.id.q2);
        question4 = (RadioGroup) findViewById(R.id.q4);
        question6 = (RadioGroup) findViewById(R.id.q6);

        //Getting checkboxes objects
        question3_a = (CheckBox) findViewById(R.id.q3_a_cb);
        question3_b = (CheckBox) findViewById(R.id.q3_b_cb);
        question3_c = (CheckBox) findViewById(R.id.q3_c_cb);
        question3_d = (CheckBox) findViewById(R.id.q3_d_cb);

        question5_a = (CheckBox) findViewById(R.id.q5_a_cb);
        question5_b = (CheckBox) findViewById(R.id.q5_b_cb);
        question5_c = (CheckBox) findViewById(R.id.q5_c_cb);
        question5_d = (CheckBox) findViewById(R.id.q5_d_cb);
    }

    public void calculateResult(View view) {
        gaindMarks = 0;
        int q1SelectedId, q2SelectedId, q4SelectedId, q6SelectedId;
        q1SelectedId = question1.getCheckedRadioButtonId();
        q2SelectedId = question2.getCheckedRadioButtonId();
        q4SelectedId = question4.getCheckedRadioButtonId();
        q6SelectedId = question6.getCheckedRadioButtonId();
        //checking if all questions are attempted
        if (q1SelectedId == -1 || q2SelectedId == -1 || q4SelectedId == -1 || q6SelectedId == -1
                || (!question3_a.isChecked() && !question3_b.isChecked() && !question3_c.isChecked() && !question3_d.isChecked())
                || (!question5_a.isChecked() && !question5_b.isChecked() && !question5_c.isChecked() && !question5_d.isChecked())) {
            Toast attemptErrorMsg = Toast.makeText(this, "Attempt all question", Toast.LENGTH_SHORT);
            attemptErrorMsg.show();
            return;
        } else {
            if (q1SelectedId == R.id.q1_b_rb) {
                gaindMarks++;
            }
            if (q2SelectedId == R.id.q2_b_rb) {
                gaindMarks++;
            }
            if (q4SelectedId == R.id.q4_b_rb) {
                gaindMarks++;
            }
            if (q6SelectedId == R.id.q6_c_rb) {
                gaindMarks++;
            }
            if (question3_a.isChecked() && question3_b.isChecked() && !question3_c.isChecked() && !question3_d.isChecked()) {
                gaindMarks++;
            }
            if (question5_a.isChecked() && question5_b.isChecked() && !question5_c.isChecked() && !question5_d.isChecked()) {
                gaindMarks++;
            }

            Toast result = Toast.makeText(this, "You gained " + gaindMarks + " marks out of " + totalMarks, Toast.LENGTH_LONG);
            result.show();
        }

    }
}
